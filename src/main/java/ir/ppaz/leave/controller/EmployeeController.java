package ir.ppaz.leave.controller;

import ir.ppaz.leave.code.LeaveCodes;
import ir.ppaz.leave.core.LeaveException;
import ir.ppaz.leave.dto.EmployeeNewEditDto;
import ir.ppaz.leave.dto.LoginDto;
import ir.ppaz.leave.model.Employee;
import ir.ppaz.leave.service.EmployeeService;
import ir.ppaz.leave.service.TeamService;
import ir.ppaz.leave.tool.SessionTools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Component
@RequestMapping(path = "/employee")
public class EmployeeController {

    Logger logger = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    EmployeeService employeeService;

    @Autowired
    TeamService teamService;

    @GetMapping(path = "/login")
    public ModelAndView login(HttpServletRequest servletRequest){
        return new ModelAndView("employee/login", new HashMap(){{
            put("login", new LoginDto());
        }});
    }

    @PostMapping(path = "/login")
    public ModelAndView login(@ModelAttribute("login") LoginDto loginDto, HttpServletRequest servletRequest){
        try {
            loginDto.setIp(servletRequest.getRemoteAddr());
            Employee employee = employeeService.login(loginDto);
            SessionTools.setCurrentUser(servletRequest,employee);
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap(){{
                        put("code",e.getCode());
                        put("message",e.getMessage());
            }});
        }
        return new ModelAndView("redirect:/");
    }

    @GetMapping("/logout")
    public ModelAndView logout(HttpServletRequest servletRequest) {
        servletRequest.removeAttribute(LeaveCodes.USER_SESSION);
        return new ModelAndView("redirect:/employee/login");
    }

    @GetMapping(path = "/list")
    public ModelAndView listAll(HttpServletRequest servletRequest){
        //TODO: LOGGING
        logger.info("=8=کاربر وارد قسمت لیست کارمندان شد==");

        System.out.println("=8=کاربر وارد قسمت لیست کارمندان شد==" + new Date());

        if(Objects.isNull(SessionTools.getCurrentUser(servletRequest))){
            return new ModelAndView("redirect:/employee/login");
        }

        //TODO: قراره فقط خودش و کارمندای زیر مجموعش را ببینه
        try {
            List<Employee> employeeList = employeeService.listEmployeeAndAllSub(SessionTools.getCurrentUser(servletRequest).getId());
            return new ModelAndView("employee/listAll", new HashMap(){{
                put("employeeList",employeeList);
            }});
        } catch (LeaveException e) {
            logger.error("=8=" + e.getMessage() + "==");
            System.out.println("=8=" + e.getMessage() + "==" + new Date());
            return new ModelAndView("core/error",
                    new HashMap(){{
                        put("code",e.getCode());
                        put("message",e.getMessage());
                    }});
        }
    }

    @GetMapping(path = "/add")
    public ModelAndView add(HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        EmployeeNewEditDto employeeNewEditDto = new EmployeeNewEditDto();
        employeeNewEditDto.setManagerId(SessionTools.getCurrentUser(servletRequest).getId());
        HashMap<String, String> teams = teamService.findAllForCombobox();
        return new ModelAndView("employee/newEdit", new HashMap(){{
            put("employee", employeeNewEditDto);
            put("teams",teams);
        }});
    }

    @PostMapping(path = "/add")
    public ModelAndView add(@ModelAttribute("employee") EmployeeNewEditDto employeeDto, HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        try {
            employeeService.save(employeeDto);
            return new ModelAndView("redirect:/employee/list");
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap(){{
                        put("code",e.getCode());
                        put("message",e.getMessage());
                    }});
        }
    }

}
