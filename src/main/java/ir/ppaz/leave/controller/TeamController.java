package ir.ppaz.leave.controller;

import ir.ppaz.leave.core.LeaveException;
import ir.ppaz.leave.dto.TeamNewEditDto;
import ir.ppaz.leave.model.Team;
import ir.ppaz.leave.service.TeamService;
import ir.ppaz.leave.tool.SessionTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Component
@RequestMapping(path = "/team")
public class TeamController {

    @Autowired
    TeamService teamService;

    @GetMapping(path = "/list")
    public ModelAndView listAll(HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        List<Team> teamList = teamService.finAll();
        return new ModelAndView("team/listAll", new HashMap() {{
            put("teamList", teamList);
        }});
    }

    @GetMapping(path = "/list1")
    public ModelAndView listAll() {
        List<Team> teamList = teamService.finAll();
        return new ModelAndView("team/listAll", new HashMap() {{
            put("teamList", teamList);
        }});
    }

    @GetMapping(path = "/add")
    public ModelAndView add(HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        TeamNewEditDto teamNewEditDto = new TeamNewEditDto();
        return new ModelAndView("team/newEdit", new HashMap() {{
            put("team", teamNewEditDto);
        }});
    }

    @PostMapping(path = "/add")
    public ModelAndView add(@ModelAttribute("team") TeamNewEditDto teamDto, HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        try {
            teamService.save(teamDto);
            return new ModelAndView("redirect:/team/list");
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap() {{
                        put("code", e.getCode());
                        put("message", e.getMessage());
                    }});
        }
    }

    @GetMapping("/edit/{id}")
    public ModelAndView edit(@PathVariable("id") Integer id, HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        try {
            TeamNewEditDto team = teamService.getTeamDtoById(id);
            return new ModelAndView("team/newEdit", new HashMap() {{
                put("team", team);
            }});
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap() {{
                        put("code", e.getCode());
                        put("message", e.getMessage());
                    }});
        }
    }

    @GetMapping("/delete/{id}")
    public ModelAndView delete(@PathVariable("id") Integer id, HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        try {
            teamService.delete(id);
            return new ModelAndView("redirect:/team/list");
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap() {{
                        put("code", e.getCode());
                        put("message", e.getMessage());
                    }});
        }
    }

}
