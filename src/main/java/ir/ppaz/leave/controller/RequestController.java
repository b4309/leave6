package ir.ppaz.leave.controller;

import ir.ppaz.leave.core.LeaveException;
import ir.ppaz.leave.dto.RequestCommentNewEditDto;
import ir.ppaz.leave.dto.RequestNewEditDto;
import ir.ppaz.leave.model.Request;
import ir.ppaz.leave.model.RequestComment;
import ir.ppaz.leave.service.RequestService;
import ir.ppaz.leave.tool.SessionTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@Component
@RequestMapping(path = "/request")
public class RequestController {

    @Autowired
    RequestService requestService;

    @GetMapping(path = "/list")
    public ModelAndView listRequest(HttpServletRequest servletRequest){
        if(Objects.isNull(SessionTools.getCurrentUser(servletRequest))){
            return new ModelAndView("redirect:/employee/login");
        }

        //TODO: قراره فقط خودش و کارمندای زیر مجموعش را ببینه
        try {
            List<Request> requestList = requestService.listRequestOfEmployeeAndAllSub(SessionTools.getCurrentUser(servletRequest).getId());
            return new ModelAndView("request/listAll", new HashMap(){{
                put("requestList",requestList);
                put("employee",SessionTools.getCurrentUser(servletRequest));
            }});
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap(){{
                        put("code",e.getCode());
                        put("message",e.getMessage());
                    }});
        }
    }

    @GetMapping(path = "/list/{requestId}")
    public ModelAndView listRequestComment(@PathVariable("requestId") Integer requestId, HttpServletRequest servletRequest){
        if(Objects.isNull(SessionTools.getCurrentUser(servletRequest))){
            return new ModelAndView("redirect:/employee/login");
        }

        try {
            List<RequestComment> requestCommentList = requestService.listComments(requestId);
            return new ModelAndView("request/listAllComment", new HashMap(){{
                put("commentList",requestCommentList);
                put("requestId",requestId);
            }});
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap(){{
                        put("code",e.getCode());
                        put("message",e.getMessage());
                    }});
        }
    }

    @GetMapping(path = "/add")
    public ModelAndView addRequest(HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        RequestNewEditDto request = new RequestNewEditDto();
        request.setEmployeeId(SessionTools.getCurrentUser(servletRequest).getId());
        return new ModelAndView("request/newEdit", new HashMap(){{
            put("request", request);
        }});
    }

    @GetMapping(path = "/add/{requestId}")
    public ModelAndView addRequestComment(@PathVariable("requestId") Integer requestId, HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        RequestCommentNewEditDto requestCommentNewEditDto = new RequestCommentNewEditDto();
        requestCommentNewEditDto.setRequestId(requestId);
        return new ModelAndView("request/newEditComment", new HashMap(){{
            put("comment", requestCommentNewEditDto);
            put("requestId",requestId);
        }});
    }

    @PostMapping(path = "/add")
    public ModelAndView addRequest(@ModelAttribute("request") RequestNewEditDto requestDto, HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        try {
            requestService.save(requestDto);
            return new ModelAndView("redirect:/request/list");
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap(){{
                        put("code",e.getCode());
                        put("message",e.getMessage());
                    }});
        }
    }

    @PostMapping(path = "/comment/add")
    public ModelAndView addRequestComment(@ModelAttribute("comment") RequestCommentNewEditDto commentDto, HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        try {
            requestService.saveComment(commentDto);
            return new ModelAndView("redirect:/request/list/" + commentDto.getRequestId());
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap(){{
                        put("code",e.getCode());
                        put("message",e.getMessage());
                    }});
        }
    }

    @GetMapping("/edit/{id}")
    public ModelAndView editRequest(@PathVariable("id") Integer id, HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        try {
            RequestNewEditDto request = requestService.getRequestDtoById(id);
            return new ModelAndView("team/newEdit", new HashMap() {{
                put("request", request);
            }});
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap() {{
                        put("code", e.getCode());
                        put("message", e.getMessage());
                    }});
        }
    }

    @GetMapping("/delete/{id}")
    public ModelAndView deleteRequest(@PathVariable("id") Integer id, HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        try {
            requestService.deleteRequest(id);
            return new ModelAndView("redirect:/request/list");
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap() {{
                        put("code", e.getCode());
                        put("message", e.getMessage());
                    }});
        }
    }

    @GetMapping("/comment/delete/{id}/{requestId}")
    public ModelAndView deleteRequestComment(@PathVariable("id") Integer id,@PathVariable("requestId") Integer requestId, HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        try {
            requestService.deleteComment(id);
            return new ModelAndView("redirect:/request/list/"+requestId);
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap() {{
                        put("code", e.getCode());
                        put("message", e.getMessage());
                    }});
        }
    }

    @GetMapping("/accept/{id}")
    public ModelAndView acceptRequest(@PathVariable("id") Integer id, HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        try {
            requestService.accept(id);
            return new ModelAndView("redirect:/request/list");
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap() {{
                        put("code", e.getCode());
                        put("message", e.getMessage());
                    }});
        }
    }

    @GetMapping("/reject/{id}")
    public ModelAndView rejectRequest(@PathVariable("id") Integer id, HttpServletRequest servletRequest) {
        if (Objects.isNull(SessionTools.getCurrentUser(servletRequest))) {
            return new ModelAndView("redirect:/employee/login");
        }
        try {
            requestService.reject(id);
            return new ModelAndView("redirect:/request/list");
        } catch (LeaveException e) {
            return new ModelAndView("core/error",
                    new HashMap() {{
                        put("code", e.getCode());
                        put("message", e.getMessage());
                    }});
        }
    }

}
