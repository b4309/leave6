package ir.ppaz.leave.controller;

import ir.ppaz.leave.code.LeaveCodes;
import ir.ppaz.leave.dto.LoginDto;
import ir.ppaz.leave.model.Employee;
import ir.ppaz.leave.tool.SessionTools;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Objects;

@Component
@RequestMapping(path = "/")
public class HomeController {

    @GetMapping(path = "/")
    public ModelAndView index(HttpServletRequest servletRequest){
        if(Objects.isNull(SessionTools.getCurrentUser(servletRequest))){
            return new ModelAndView("redirect:/employee/login");
        }

        return new ModelAndView("home/index");
    }



}
