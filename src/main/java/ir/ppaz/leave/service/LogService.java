package ir.ppaz.leave.service;

import ir.ppaz.leave.core.LeaveException;
import ir.ppaz.leave.dto.TeamNewEditDto;
import ir.ppaz.leave.model.Log;
import ir.ppaz.leave.model.Team;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public interface LogService {
    void save(Log log);
}
