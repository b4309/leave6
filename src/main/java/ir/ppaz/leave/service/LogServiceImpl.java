package ir.ppaz.leave.service;

import ir.ppaz.leave.model.Log;
import ir.ppaz.leave.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LogServiceImpl implements LogService {

    @Autowired
    LogRepository repository;

    @Override
    public void save(Log log) {
        //TODO: Send Email to PO + Terello
        repository.save(log);
    }
}
