package ir.ppaz.leave.service;

import ir.ppaz.leave.code.ErrorCodes;
import ir.ppaz.leave.code.LeaveCodes;
import ir.ppaz.leave.core.LeaveException;
import ir.ppaz.leave.dto.RequestCommentNewEditDto;
import ir.ppaz.leave.dto.RequestNewEditDto;
import ir.ppaz.leave.model.Request;
import ir.ppaz.leave.model.RequestComment;
import ir.ppaz.leave.repository.RequestCommentRepository;
import ir.ppaz.leave.repository.RequestRepository;
import ir.ppaz.leave.tool.ValidationTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RequestServiceImpl implements RequestService {

    @Autowired
    RequestRepository requestRepository;

    @Autowired
    RequestCommentRepository commentRepository;

    @Autowired
    EmployeeService employeeService;

    @Override
    public Request findById(Integer id) throws LeaveException {
        return requestRepository.findById(id).orElseThrow(() -> new LeaveException(ErrorCodes.REQUEST_NOT_FOUND, "درخواستی با این مشخصات یافت نشد"));
    }

    @Override
    public List<Request> listRequestOfEmployeeAndAllSub(Integer employeeId) throws LeaveException {
        return requestRepository.findRequestOfEmployeeAndAllSub(employeeId);
    }

    @Override
    public void save(RequestNewEditDto requestDto) throws LeaveException {
        validateSaveDto(requestDto);
        requestDto.setStatus(LeaveCodes.REQUEST_STATUS_CREATE);
        Request request = convertSaveDtoToEntity(requestDto);
        requestRepository.save(request);
    }

    private Request convertSaveDtoToEntity(RequestNewEditDto requestDto) {
        return Request.builder()
                .id(requestDto.getId())
                .startDate(requestDto.getStartDate())
                .startTime(requestDto.getStartTime())
                .endDate(requestDto.getEndDate())
                .endTime(requestDto.getEndTime())
                .status(requestDto.getStatus())
                .description(requestDto.getDescription())
                .employee(employeeService.getEmployeeById(requestDto.getEmployeeId()))
                .build();
    }

    private void validateSaveDto(RequestNewEditDto requestDto) throws LeaveException {
        ValidationTools.validateTime(requestDto.getStartTime(), "ساعت شروع");
        ValidationTools.validateTime(requestDto.getEndTime(), "ساعت پایان");
        ValidationTools.validateDate(requestDto.getStartDate(), "تاریخ شروع");
        ValidationTools.validateDate(requestDto.getEndDate(), "تاریخ پایان");

        String start = requestDto.getStartDate() + " " + requestDto.getStartTime();
        String end = requestDto.getEndDate() + " " + requestDto.getEndTime();
        if (start.compareTo(end) >= 0) {
            throw new LeaveException(ErrorCodes.START_DATE_GRADER_THAN_END, "زمان و تاریخ شروع باید قبل از پایان باشد");
        }
        /*
            =0 => 2 equal 1
            <0 => 2 less 1
            >0 => 2 grader 1
        */

        List<Request> requestByEmployeeId = requestRepository.findRequestByEmployeeId(requestDto.getEmployeeId()).stream().filter(request -> {
            if (request.getStatus().equals(LeaveCodes.REQUEST_STATUS_REJECT)) {
                return false;
            }
            String startDB = request.getStartDate() + " " + request.getStartTime();
            String endDB = request.getEndDate() + " " + request.getEndTime();
            if (startDB.compareTo(start) <= 0 && endDB.compareTo(end) >= 0) {
                return true;
            }
            if (startDB.compareTo(start) <= 0 && endDB.compareTo(end) <= 0 && endDB.compareTo(start) >= 0) {
                return true;
            }
            if (startDB.compareTo(start) >= 0 && endDB.compareTo(end) >= 0 && startDB.compareTo(endDB) <= 0) {
                return true;
            }
            if (startDB.compareTo(start) >= 0 && endDB.compareTo(end) <= 0) {
                return true;
            }
            return false;
        }).collect(Collectors.toList());
        if (requestByEmployeeId.size() > 0) {
            throw new LeaveException(ErrorCodes.REQUEST_ALREADY_EXIST, "قبلا یک در خواست برای این فرد در این زمان ثبت شده است.");
        }

    }

    @Override
    public RequestNewEditDto getRequestDtoById(Integer id) throws LeaveException {
        Request request = findById(id);
        RequestNewEditDto requestNewEditDto = convertRequestToDto(request);
        return requestNewEditDto;
    }

    private RequestNewEditDto convertRequestToDto(Request request) {
        return RequestNewEditDto.builder()
                .id(request.getId())
                .startDate(request.getStartDate())
                .startTime(request.getStartTime())
                .endDate(request.getEndDate())
                .endTime(request.getEndTime())
                .status(request.getStatus())
                .description(request.getDescription())
                .employeeId(request.getEmployee().getId())
                .build();
    }

    @Override
    public void deleteRequest(Integer id) throws LeaveException {
        Request request = findById(id);
        validateDelete(request);
        requestRepository.delete(request);
    }

    private void validateDelete(Request request) throws LeaveException {
        if (request.getStatus().equals(LeaveCodes.REQUEST_STATUS_ACCEPT) || request.getStatus().equals(LeaveCodes.REQUEST_STATUS_REJECT)) {
            throw new LeaveException(ErrorCodes.DELETE_REQUEST_FAILED, "قابلیت حذف درخواست های رد شده یا پذیرفته شده وجود ندارد");
        }
        if (listComments(request.getId()).size() > 0) {
            throw new LeaveException(ErrorCodes.DELETE_REQUEST_FAILED, "برای این درخواست کامنت هایی نوشته شده است");
        }
    }

    @Override
    public void accept(Integer id) throws LeaveException {
        Request request = findById(id);
        if (!request.getStatus().equals(LeaveCodes.REQUEST_STATUS_CREATE)) {
            throw new LeaveException(ErrorCodes.REQUEST_IN_BAD_STATUS, "این عملیات صرفا بر روی درخواست های جدید قابل اجرا است");
        }
        request.setStatus(LeaveCodes.REQUEST_STATUS_ACCEPT);
        requestRepository.save(request);
    }

    @Override
    public void reject(Integer id) throws LeaveException {
        Request request = findById(id);
        if (!request.getStatus().equals(LeaveCodes.REQUEST_STATUS_CREATE)) {
            throw new LeaveException(ErrorCodes.REQUEST_IN_BAD_STATUS, "این عملیات صرفا بر روی درخواست های جدید قابل اجرا است");
        }
        request.setStatus(LeaveCodes.REQUEST_STATUS_REJECT);
        requestRepository.save(request);
    }

    @Override
    public List<RequestComment> listComments(Integer requestId) throws LeaveException {
        Request request = findById(requestId);
        List<RequestComment> comments = commentRepository.findAllByRequest(request);
        return comments;
    }

    @Override
    public void saveComment(RequestCommentNewEditDto commentDto) throws LeaveException {
        validateCommentDto(commentDto);
        RequestComment comment = convertCommentDtoToEntity(commentDto);
        commentRepository.save(comment);
    }

    private RequestComment convertCommentDtoToEntity(RequestCommentNewEditDto commentDto) throws LeaveException {
        return RequestComment.builder()
                .id(commentDto.getId())
                .text(commentDto.getText())
                .subject(commentDto.getSubject())
                .dateTime(commentDto.getDateTime())
                .sender(commentDto.getSender())
                .request(findById(commentDto.getRequestId()))
                .build();
    }

    private void validateCommentDto(RequestCommentNewEditDto commentDto) throws LeaveException {
        ValidationTools.validateEmptyField(commentDto.getSubject(), "موضوع");
        ValidationTools.validateEmptyField(commentDto.getText(), "متن");
        ValidationTools.validateDateTime(commentDto.getDateTime(), "زمان");
    }

    @Override
    public void deleteComment(Integer id) throws LeaveException {
        RequestComment comment = findCommentById(id);
        commentRepository.delete(comment);
    }

    private RequestComment findCommentById(Integer id) throws LeaveException {
        return commentRepository.findById(id).orElseThrow(() -> new LeaveException(ErrorCodes.REQUEST_COMMENT_NOT_FOUND, "کامنتی با این مشخصات یافت نشد"));
    }

}
