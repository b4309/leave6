package ir.ppaz.leave.service;

import ir.ppaz.leave.core.LeaveException;
import ir.ppaz.leave.dto.RequestCommentNewEditDto;
import ir.ppaz.leave.dto.RequestNewEditDto;
import ir.ppaz.leave.model.Request;
import ir.ppaz.leave.model.RequestComment;
import ir.ppaz.leave.repository.RequestRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface RequestService {
    //Request
    Request findById(Integer id) throws LeaveException;

    List<Request> listRequestOfEmployeeAndAllSub(Integer employeeId) throws LeaveException;

    void save(RequestNewEditDto requestDto) throws LeaveException;

    RequestNewEditDto getRequestDtoById(Integer id) throws LeaveException;

    void deleteRequest(Integer id) throws LeaveException;

    void accept(Integer id) throws LeaveException;

    void reject(Integer id) throws LeaveException;
    //RequestComment
    List<RequestComment> listComments(Integer requestId) throws LeaveException;

    void saveComment(RequestCommentNewEditDto commentDto) throws LeaveException;

    void deleteComment(Integer id) throws LeaveException;

}
