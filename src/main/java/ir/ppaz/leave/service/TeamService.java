package ir.ppaz.leave.service;

import ir.ppaz.leave.core.LeaveException;
import ir.ppaz.leave.dto.TeamNewEditDto;
import ir.ppaz.leave.model.Team;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public interface TeamService {

    HashMap<String, String> findAllForCombobox();

    Team getTeamById(Integer id) throws LeaveException;

    TeamNewEditDto getTeamDtoById(Integer id) throws LeaveException;

    Team getTeamByIdIgnoreFound(Integer id);

    List<Team> finAll();

    void save(TeamNewEditDto teamDto) throws LeaveException;

    void delete(Integer id) throws LeaveException;

    Integer getSumAge(Integer teamId);
}
