package ir.ppaz.leave.service;

import ir.ppaz.leave.code.ErrorCodes;
import ir.ppaz.leave.core.LeaveException;
import ir.ppaz.leave.dto.EmployeeNewEditDto;
import ir.ppaz.leave.dto.LoginDto;
import ir.ppaz.leave.model.Employee;
import ir.ppaz.leave.model.Log;
import ir.ppaz.leave.model.Team;
import ir.ppaz.leave.repository.EmployeeRepository;
import ir.ppaz.leave.tool.EncryptTools;
import ir.ppaz.leave.tool.ValidationTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    LogService logService;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    TeamService teamService;

    @Override
    public Employee login(LoginDto loginDto) throws LeaveException {
        validateLogin(loginDto);
        Employee employee = employeeRepository.login(loginDto.getUserName(), EncryptTools.encryptPassword(loginDto.getPassword()));
        if (Objects.isNull(employee)) {
            logService.save(Log.builder().code(1).action("Employee.login").comment("ورود نا موفق").dateTime(new Date()).username(loginDto.getUserName()).ip(loginDto.getIp()).build());
            throw new LeaveException(ErrorCodes.USER_NOT_FOUND, "کاربری با این مشخصات در سیستم ثبت نشده است");
        }
        logService.save(Log.builder().code(1).action("Employee.login").comment("ورود موفق").dateTime(new Date()).username(loginDto.getUserName()).ip(loginDto.getIp()).build());
        return employee;
    }

    @Override
    public List<Employee> listEmployeeAndAllSub(Integer employeeId) throws LeaveException {
        Employee employee = employeeRepository.findById(employeeId).orElseThrow(
                () -> new LeaveException(ErrorCodes.EMPLOYEE_NOT_FOUND, "کارمندی با این مشخصات یافت نشد")
        );
        List<Employee> allThatHeIsManager = employeeRepository.findAllThatHeIsManager(employeeId);
        allThatHeIsManager.add(0, employee);
        return allThatHeIsManager;
    }

    @Override
    public void save(EmployeeNewEditDto employeeDto) throws LeaveException {
        validateSave(employeeDto);
        Employee employee = convertFromDtoToEmployee(employeeDto);
        try {
            employeeRepository.save(employee);
        }catch (Exception e) {
            throw new LeaveException(ErrorCodes.DATABASE_ERROR , "خطایی در ذخیره کارمند رخ داده است");
        }
    }

    @Override
    public Employee getEmployeeById(Integer id) {
        Employee employee = employeeRepository.findById(id).orElseGet(null);
        return employee;
    }

    @Override
    public Employee getEmployeeByPersonCode(String personCode) {
        Employee employeeByPersonalCode = employeeRepository.findEmployeeByPersonalCode(personCode);
        return employeeByPersonalCode;
    }

    @Override
    public Long countEmployeeOfTeam(Team team) {
        Long count = employeeRepository.countEmployeeByTeam(team);
        return count;
    }

    private Employee convertFromDtoToEmployee(EmployeeNewEditDto employeeDto) throws LeaveException {
        Employee employee = new Employee();
        employee.setName(employeeDto.getName());
        employee.setFamily(employeeDto.getFamily());
        employee.setBirthDate(employeeDto.getBirthDate());
        employee.setPersonalCode(employeeDto.getPersonalCode());
        employee.setNationalId(employeeDto.getNationalId());
        employee.setGender(employeeDto.getGender());
        employee.setIsAdmin(employeeDto.getIsAdmin());
        employee.setPassword(EncryptTools.encryptPassword(employeeDto.getPassword()));
        employee.setTeam(teamService.getTeamById(employeeDto.getTeamId()));
        if(Objects.nonNull(employeeDto.getManagerId())) {
            employee.setManager(getEmployeeById(employeeDto.getManagerId()));
        }
        return employee;
    }

    private void validateSave(EmployeeNewEditDto employeeDto) throws LeaveException {
        if (Objects.isNull(employeeDto)) {
            throw new LeaveException(ErrorCodes.EMPLOYEE_SAVE_FORM_IS_NULL, "فرم به درستی تکمیل نشده است");
        }
        ValidationTools.validateEmptyField(employeeDto.getName(),"نام");
        ValidationTools.validateEmptyField(employeeDto.getFamily(),"نام خانوادگی");
        ValidationTools.validateEmptyField(employeeDto.getNationalId(),"کد ملی");
        ValidationTools.validateEmptyField(employeeDto.getPersonalCode(),"کد پرسنلی");
        Employee employeeByPersonCode = getEmployeeByPersonCode(employeeDto.getPersonalCode());
        if(Objects.nonNull(employeeByPersonCode)){
            throw new LeaveException(ErrorCodes.PERSON_CODE_NOT_UNIQUE, "قردی با این کد پرسنلی قبلا ثبت شده است");
        }

        //TODO:نوشتن بقیه ولیدیشن ها
        ValidationTools.validationFieldLengthLess(employeeDto.getPassword(),6,"رمز عبور");
        ValidationTools.validationFieldLengthLess(employeeDto.getNationalId(),10,"کد ملی");
    }

    private void validateLogin(LoginDto loginDto) throws LeaveException {
        if (Objects.isNull(loginDto)) {
            throw new LeaveException(ErrorCodes.LOGIN_FORM_IS_NULL, "فرم به درستی تکمیل نشده است");
        }
        ValidationTools.validateEmptyField(loginDto.getUserName(),"نام کاربری");
        ValidationTools.validateEmptyField(loginDto.getPassword(),"رمز عبور");

        ValidationTools.validationFieldLengthLess(loginDto.getPassword(),6,"رمز عبور");

    }
}
