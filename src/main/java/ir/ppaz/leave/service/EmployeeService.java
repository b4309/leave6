package ir.ppaz.leave.service;

import ir.ppaz.leave.core.LeaveException;
import ir.ppaz.leave.dto.EmployeeNewEditDto;
import ir.ppaz.leave.dto.LoginDto;
import ir.ppaz.leave.model.Employee;
import ir.ppaz.leave.model.Team;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface EmployeeService {

    Employee login(LoginDto loginDto) throws LeaveException;

    List<Employee> listEmployeeAndAllSub(Integer employeeId) throws LeaveException;

    void save(EmployeeNewEditDto employeeDto) throws LeaveException;

    Employee getEmployeeById(Integer id);

    Employee getEmployeeByPersonCode(String personCode);

    Long countEmployeeOfTeam(Team team);
}
