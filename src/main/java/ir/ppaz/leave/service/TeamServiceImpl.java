package ir.ppaz.leave.service;

import ir.ppaz.leave.code.ErrorCodes;
import ir.ppaz.leave.core.LeaveException;
import ir.ppaz.leave.dto.TeamNewEditDto;
import ir.ppaz.leave.model.Team;
import ir.ppaz.leave.repository.TeamRepository;
import ir.ppaz.leave.tool.ValidationTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class TeamServiceImpl implements TeamService {

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    EmployeeService employeeService;

    @Override
    public HashMap<String, String> findAllForCombobox() {
        HashMap<String, String> teams = new HashMap<>();
        teamRepository.findAll().forEach(team -> teams.put(team.getCode(), team.getName()));
        return teams;
    }

    @Override
    public Team getTeamById(Integer id) throws LeaveException {
        Team team = teamRepository.findById(id).orElseGet(null);
        if (Objects.isNull(team)) {
            throw new LeaveException(ErrorCodes.TEAM_NOT_FOUND, "تیمی با این شناسه در سیستم تعریف نشده است");
        }
        return team;
    }

    @Override
    public TeamNewEditDto getTeamDtoById(Integer id) throws LeaveException {
        Team team = getTeamById(id);
        TeamNewEditDto teamDto = TeamNewEditDto.builder()
                .id(team.getId())
                .name(team.getName())
                .code(team.getCode())
                .build();
        return teamDto;
    }

    @Override
    public Team getTeamByIdIgnoreFound(Integer id) {
        return teamRepository.findById(id).orElse(null);
    }

    @Override
    public List<Team> finAll() {
        Iterable<Team> all = teamRepository.findAll();
        return (List<Team>) all;
    }

    @Override
    public void save(TeamNewEditDto teamDto) throws LeaveException {
        validateSaveEdit(teamDto);
        Team team = convertDtoToEntity(teamDto);
        teamRepository.save(team);
    }

    @Override
    public void delete(Integer id) throws LeaveException {
        Team team = getTeamById(id);
        validateDelete(team);
        teamRepository.delete(team);
    }

    @Override
    public Integer getSumAge(Integer teamId) {
        return 300;
    }

    private void validateDelete(Team team) throws LeaveException {
        Long countEmployeeOfTeam = employeeService.countEmployeeOfTeam(team);
        if(countEmployeeOfTeam != 0L){
            throw new LeaveException(ErrorCodes.TEAM_HAS_EMPLOYEE, "این تیم دارای کارمندانی هست");
        }
    }

    private Team convertDtoToEntity(TeamNewEditDto teamDto) {
        Team.TeamBuilder teamBuilder = Team.builder()
                .name(teamDto.getName())
                .code(teamDto.getCode());
        if (Objects.nonNull(teamDto.getId())) {
            teamBuilder.id(teamDto.getId());
        }
        return teamBuilder.build();
    }

    private void validateSaveEdit(TeamNewEditDto teamDto) throws LeaveException {
        ValidationTools.validateEmptyField(teamDto.getName(), "نام تیم");
        ValidationTools.validateEmptyField(teamDto.getCode(), "کد تیم");
    }
}
