package ir.ppaz.leave.core;

public class LeaveException extends Exception {

    private String code;
    private String message;

    public LeaveException(String code,String message){
        super(code + ":" + message);
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
