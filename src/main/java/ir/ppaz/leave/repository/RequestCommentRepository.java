package ir.ppaz.leave.repository;

import ir.ppaz.leave.model.Request;
import ir.ppaz.leave.model.RequestComment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestCommentRepository extends CrudRepository<RequestComment,Integer> {

    List<RequestComment> findAllByRequest(Request request);

}
