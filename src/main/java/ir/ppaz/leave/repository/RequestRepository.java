package ir.ppaz.leave.repository;

import ir.ppaz.leave.model.Request;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RequestRepository extends CrudRepository<Request,Integer> {

    @Query("select r from Request r where r.employee.id = ?1 or r.employee.manager.id = ?1")
    List<Request> findRequestOfEmployeeAndAllSub(Integer employeeId);

    @Query("select r from Request r where r.employee.id = ?1")
    List<Request> findRequestByEmployeeId(Integer employeeId);

}
