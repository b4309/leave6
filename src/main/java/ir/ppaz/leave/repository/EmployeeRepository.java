package ir.ppaz.leave.repository;

import ir.ppaz.leave.model.Employee;
import ir.ppaz.leave.model.Team;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee, Integer> {

    @Query("select e from Employee e where e.personalCode=?1 and e.password=?2")
    Employee login(String userName, String password);

    @Query("select e from Employee e where e.manager.id=?1")
    List<Employee> findAllThatHeIsManager(Integer employeeId);

    Employee findEmployeeByPersonalCode(String personalCode);

    Long countEmployeeByTeam(Team team);

}
