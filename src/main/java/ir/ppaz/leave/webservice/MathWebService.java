package ir.ppaz.leave.webservice;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api/math/")
public class MathWebService {

    @GetMapping(path = "/sum/{a}/{b}")
    public Integer sum(@PathVariable("a") int a, @PathVariable("b") int b) {
        return a+b;
    }

}
