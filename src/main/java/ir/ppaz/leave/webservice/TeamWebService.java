package ir.ppaz.leave.webservice;

import ir.ppaz.leave.code.ErrorCodes;
import ir.ppaz.leave.core.LeaveException;
import ir.ppaz.leave.dto.LoginDto;
import ir.ppaz.leave.dto.TeamNewEditDto;
import ir.ppaz.leave.model.Employee;
import ir.ppaz.leave.model.Team;
import ir.ppaz.leave.service.EmployeeService;
import ir.ppaz.leave.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/leave/team")
public class TeamWebService {

    @Autowired
    TeamService teamService;

    @Autowired
    EmployeeService employeeService;

    @PostMapping(path = "/add/{name}/{code}/{username}/{password}")
    public String add(@PathVariable("name") String name,@PathVariable("code") String code,@PathVariable("username") String username,@PathVariable("password") String password) {
        try {
            Employee login = employeeService.login(LoginDto.builder().userName(username).password(password).build());
            if (Objects.isNull(login)) {
                return ErrorCodes.USER_NOT_FOUND + "::" + "کاربری با این مشخصات برای سیستم تعریف نشده است";
            }
        } catch (LeaveException e) {
            return e.getCode() + "::" + e.getMessage();
        }

        try {
            teamService.save(TeamNewEditDto.builder().name(name).code(code).build());
            return ErrorCodes.SUCCESS + "::" + "تیم با موفقیت ثبت شد";
        } catch (LeaveException e) {
            return e.getCode() + "::" + e.getMessage();
        }
    }

    @GetMapping(path = "/list/{username}/{password}", produces = "application/json")
    public ResponseEntity<Object> list(@PathVariable("username") String username,@PathVariable("password") String password) {
        try {
            Employee login = employeeService.login(LoginDto.builder().userName(username).password(password).build());
            if (Objects.isNull(login)) {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new HashMap(){{
                    put("code",ErrorCodes.USER_NOT_FOUND);
                    put("message","کاربری با این مشخصات برای سیستم تعریف نشده است");
                }});
            }
        } catch (LeaveException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new HashMap(){{
                put("code",e.getCode());
                put("message",e.getMessage());
            }});
        }

        List<Team> teams = teamService.finAll();
        List<HashMap<String,String>> jsonTeams = new ArrayList<>();
        teams.forEach(team -> {
            HashMap<String,String> jsonObject = new HashMap<>();
            jsonObject.put("id", String.valueOf(team.getId()));
            jsonObject.put("name",team.getName());
            jsonObject.put("code",team.getCode());
            jsonTeams.add(jsonObject);
        });
        return ResponseEntity.status(HttpStatus.OK).body(jsonTeams);
    }

}
