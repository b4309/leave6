package ir.ppaz.leave.tool;

import ir.ppaz.leave.code.LeaveCodes;
import ir.ppaz.leave.model.Employee;

import javax.servlet.http.HttpServletRequest;

public class SessionTools {

    public static Employee getCurrentUser(HttpServletRequest servletRequest){
        Employee user = (Employee) servletRequest.getSession().getAttribute(LeaveCodes.USER_SESSION);
        return user;
    }

    public static void setCurrentUser(HttpServletRequest servletRequest,Employee user){
        servletRequest.getSession().setAttribute(LeaveCodes.USER_SESSION,user);
    }

}
