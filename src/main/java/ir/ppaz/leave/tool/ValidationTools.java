package ir.ppaz.leave.tool;

import ir.ppaz.leave.code.ErrorCodes;
import ir.ppaz.leave.core.LeaveException;

import java.util.Objects;

public class ValidationTools {

    public static void validateEmptyField(String value, String title) throws LeaveException {
        if (Objects.isNull(value) || value.isEmpty()) {
            throw new LeaveException(ErrorCodes.FIELD_IS_EMPTY, title + " نمی تواند خالی باشد");
        }
    }

    public static void validationFieldLengthLess(String value, int length, String title) throws LeaveException {
        if (value.length() < length) {
            throw new LeaveException(ErrorCodes.LENGTH_LESS, "طول " + title + " نمی تواند کمتر از " + length + " کاراکتر باشد");
        }
    }

    //TODO: -LenGreater -LenEqual -NotNull -...

    public static void validateDate(String value, String title) throws LeaveException {
        //1400/07/29
        if (value.length() != 10) {
            throw new LeaveException(ErrorCodes.DATE_FORMAT_NOT_VALID, "در فیلد " + title + "قالب نوشتاری تاریخ باید به صورت YYYY/MM/DD باشد");
        }
        if (value.charAt(4) != '/' || value.charAt(7) != '/') {
            throw new LeaveException(ErrorCodes.DATE_FORMAT_NOT_VALID, "در فیلد " + title + "قالب نوشتاری تاریخ باید به صورت YYYY/MM/DD باشد");
        }
        String[] parts = value.split("/");
        if (parts.length != 3) {
            throw new LeaveException(ErrorCodes.DATE_FORMAT_NOT_VALID, "در فیلد " + title + "قالب نوشتاری تاریخ باید به صورت YYYY/MM/DD باشد");
        }
        try {
            if (Integer.parseInt(parts[0]) <= 0 || Integer.parseInt(parts[1]) <= 0 || Integer.parseInt(parts[2]) <= 0) {
                throw new LeaveException(ErrorCodes.DATE_FORMAT_NOT_VALID, "در فیلد " + title + "قالب نوشتاری تاریخ باید به صورت YYYY/MM/DD باشد");
            }
        } catch (Exception e) {
            throw new LeaveException(ErrorCodes.DATE_FORMAT_NOT_VALID, "در فیلد " + title + "قالب نوشتاری تاریخ باید به صورت YYYY/MM/DD باشد");
        }

//        a=6;
//        b=3;
//        if(a==5 && b++) => b=3;
//        if(a==5 || b++) => b=4;

//        قانون دمورگان
//        (-A && -B) = -(A || B)

//        if (a==4) if (b == 3)  => if (a==4&&b == 3)

    }

    public static void validateTime(String value, String title) throws LeaveException {
        //16:34
        if (value.length() != 5) {
            throw new LeaveException(ErrorCodes.TIME_FORMAT_NOT_VALID, "در فیلد " + title + "قالب نوشتاری ساعت باید به صورت HH:MM باشد");
        }
        if (value.charAt(2) != ':') {
            throw new LeaveException(ErrorCodes.TIME_FORMAT_NOT_VALID, "در فیلد " + title + "قالب نوشتاری ساعت باید به صورت HH:MM باشد");
        }
        String[] parts = value.split(":");
        if (parts.length != 2) {
            throw new LeaveException(ErrorCodes.TIME_FORMAT_NOT_VALID, "در فیلد " + title + "قالب نوشتاری ساعت باید به صورت HH:MM باشد");
        }
        try {
            if (Integer.parseInt(parts[0]) < 0 || Integer.parseInt(parts[1]) < 0) {
                throw new LeaveException(ErrorCodes.TIME_FORMAT_NOT_VALID, "در فیلد " + title + "قالب نوشتاری ساعت باید به صورت HH:MM باشد");
            }
        } catch (Exception e) {
            throw new LeaveException(ErrorCodes.TIME_FORMAT_NOT_VALID, "در فیلد " + title + "قالب نوشتاری ساعت باید به صورت HH:MM باشد");
        }
    }

    public static void validateDateTime(String value, String title) throws LeaveException {
        //1400/07/30 16:26
        String[] split = value.split(" ");
        if (split.length != 2) {
            throw new LeaveException(ErrorCodes.DATE_TIME_FORMAT_NOT_VALID, "در فیلد " + title + "قالب نوشتاری زمان باید به صورت YYYY/mm/DD HH:MM باشد");
        }
        validateDate(split[0], title);
        validateTime(split[1], title);
    }

}
