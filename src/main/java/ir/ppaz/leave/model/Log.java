package ir.ppaz.leave.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "Log")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Log  implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer code; //error=1,info=2,..
    private Date dateTime;
    private String ip;
    private String comment;
    private String username;
    private String action;

}
