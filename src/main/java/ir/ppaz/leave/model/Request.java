package ir.ppaz.leave.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Request")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Request implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;
    private String startDate;
    private String startTime;
    private String endDate;
    private String endTime;
    private String status;
    private String description;

    @ManyToOne
    private Employee employee;

    @OneToMany(mappedBy = "request", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<RequestComment> comments;
}
