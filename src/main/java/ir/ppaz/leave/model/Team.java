package ir.ppaz.leave.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Team")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Team implements Serializable{
    @Transient
    private static final long serialVersionUID = 6828733260151417796L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String code;

    @OneToMany(mappedBy = "team", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Employee> employees;

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
