package ir.ppaz.leave.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "Employee")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Employee implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String family;
    private String gender;
    private String nationalId;
    private String personalCode;
    private String birthDate;
    private String password;
    private Boolean isAdmin;

    @ManyToOne(fetch = FetchType.LAZY)
    private Team team;

    @ManyToOne
    private Employee manager;

    @OneToMany(mappedBy = "manager",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Employee> employees;

    @OneToMany(mappedBy = "employee",fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Request> requests;
}
