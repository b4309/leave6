package ir.ppaz.leave.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "RequestComment")
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestComment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Integer id;
    private String text;
    private String subject;
    private String dateTime;
    private String sender;

    @ManyToOne
    private Request request;
}
