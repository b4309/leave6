package ir.ppaz.leave.code;

public class ErrorCodes {

    public static String SUCCESS = "000";
    public static String LOGIN_FORM_IS_NULL = "001";
    public static String FIELD_IS_EMPTY = "002";
    public static String LENGTH_LESS = "003";
    public static String USER_NOT_FOUND = "004";
    public static String EMPLOYEE_NOT_FOUND = "005";
    public static String DATABASE_ERROR = "006";
    public static String EMPLOYEE_SAVE_FORM_IS_NULL = "007";
    public static String TEAM_NOT_FOUND = "008";
    public static String PERSON_CODE_NOT_UNIQUE = "009";
    public static String TEAM_HAS_EMPLOYEE = "010";
    public static String DATE_FORMAT_NOT_VALID = "011";
    public static String TIME_FORMAT_NOT_VALID = "012";
    public static String START_DATE_GRADER_THAN_END = "013";
    public static String REQUEST_ALREADY_EXIST = "014";
    public static String REQUEST_NOT_FOUND = "015";
    public static String DELETE_REQUEST_FAILED = "016";
    public static String DATE_TIME_FORMAT_NOT_VALID = "017";
    public static String REQUEST_COMMENT_NOT_FOUND = "018";
    public static String REQUEST_IN_BAD_STATUS = "019";

}
