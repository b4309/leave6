package ir.ppaz.leave.code;

public class LeaveCodes {

    public static String USER_SESSION = "USER_SESSION";

    public static String REQUEST_STATUS_CREATE = "create";
    public static String REQUEST_STATUS_ACCEPT = "accept";
    public static String REQUEST_STATUS_REJECT = "reject";

}
