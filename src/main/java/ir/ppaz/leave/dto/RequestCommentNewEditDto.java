package ir.ppaz.leave.dto;

import ir.ppaz.leave.model.Request;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestCommentNewEditDto {

    private Integer id;
    private String text;
    private String subject;
    private String dateTime;
    private String sender;
    private Integer requestId;
}
