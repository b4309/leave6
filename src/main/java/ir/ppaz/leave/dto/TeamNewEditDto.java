package ir.ppaz.leave.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TeamNewEditDto {

    private Integer id;
    private String name;
    private String code;

}
