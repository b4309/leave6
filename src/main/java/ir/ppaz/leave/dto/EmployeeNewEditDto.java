package ir.ppaz.leave.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeNewEditDto {

    private Integer id;
    private String name;
    private String family;
    private String gender;
    private String nationalId;
    private String personalCode;
    private String birthDate;
    private String password;
    private Boolean isAdmin;
    private Integer teamId;
    private Integer managerId;

}
