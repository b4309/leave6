package ir.ppaz.leave.dto;

import ir.ppaz.leave.model.Employee;
import ir.ppaz.leave.model.RequestComment;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RequestNewEditDto {

   private Integer id;
    private String startDate;
    private String startTime;
    private String endDate;
    private String endTime;
    private String status;
    private String description;
   private Integer employeeId;

}
