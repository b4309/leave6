<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../template/header.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
    <div class="col-md-12">
        <div class="card-header">
            <h5 class="card-title">List All User That He is Managed</h5>
            <a href="/employee/add" class="btn btn-info">New Employee</a>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class=" text-primary">
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        National Id
                    </th>
                    <th>
                        Personal Code
                    </th>
                    <th>
                        Birth Date
                    </th>
                    <th>
                        Tools
                    </th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${employeeList}" var="employee">
                    <tr>
                        <td>
                            <c:choose>
                                <c:when test="${employee.gender eq 'm'}">
                                    آقای
                                </c:when>
                                <c:otherwise>
                                    خانم
                                </c:otherwise>
                            </c:choose> ${employee.name} ${employee.family}
                        </td>
                        <td>
                                ${employee.nationalId}
                        </td>
                        <td>
                                ${employee.personalCode}
                        </td>
                        <td>
                                ${employee.birthDate}
                        </td>
                        <td>

                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<%@include file="../template/footer.jsp" %>