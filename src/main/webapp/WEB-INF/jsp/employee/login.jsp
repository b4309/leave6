<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../template/header.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="row">
    <div class="col-md-4">
        <div class="card-header">
            <h5 class="card-title">Login User</h5>
        </div>
        <div class="card-body">
            <form:form action="/employee/login" method="post" modelAttribute="login" >
                <div class="row">
                    <div class="col-md-12 pr-1">
                        <div class="form-group">
                            <form:label path="userName">User Name:</form:label>
                            <form:input path="userName" class="form-control" placeholder="User Name"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 pr-1">
                        <div class="form-group">
                            <form:label path="password">Password:</form:label>
                            <form:password path="password" class="form-control" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="update ml-auto mr-auto">
                        <button type="submit" class="btn btn-primary btn-round">Login</button>
                    </div>
                </div>
            </form:form>
        </div>
    </div>
</div>

<%@include file="../template/footer.jsp" %>