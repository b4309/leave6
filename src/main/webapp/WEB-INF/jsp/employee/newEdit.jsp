<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../template/header.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
    <div class="col-md-12">
        <div class="card-header">
            <h5 class="card-title">
                <c:choose>
                    <c:when test="${employee.id eq null}">
                        ثبت اطلاعات کارمند جدید
                    </c:when>
                    <c:otherwise>
                        ویرایش اطلاعات <c:choose>
                        <c:when test="${employee.gender eq 'm'}">
                            آقای
                        </c:when>
                        <c:otherwise>
                            خانم
                        </c:otherwise>
                    </c:choose> ${employee.name} ${employee.family}
                    </c:otherwise>
                </c:choose>
            </h5>
        </div>
        <div class="card-body">
            <form:form action="/employee/add" method="post" modelAttribute="employee" >
                <div class="row">
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <form:label path="gender">Gender:</form:label>
                            <form:select path="gender" class="form-control">
                                    <form:option value="m">آقای</form:option>
                                    <form:option value="f">خانم</form:option>
                            </form:select>
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <form:label path="name">Name:</form:label>
                            <form:input path="name" class="form-control" placeholder="Name"/>
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <form:label path="family">Family:</form:label>
                            <form:input path="family" class="form-control" placeholder="Family"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <form:label path="birthDate">Birth Date:</form:label>
                            <form:input path="birthDate" class="form-control" placeholder="Birth Date"/>
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <form:label path="nationalId">National Id:</form:label>
                            <form:input path="nationalId" class="form-control" placeholder="National Id"/>
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <form:label path="personalCode">Personal Code:</form:label>
                            <form:input path="personalCode" class="form-control" placeholder="Personal Code"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <form:label path="isAdmin">Is Admin?</form:label>
                            <form:select path="isAdmin" class="form-control">
                                <form:option value="true">بله</form:option>
                                <form:option value="false">خیر</form:option>
                            </form:select>
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <form:label path="password">Password:</form:label>
                            <form:password path="password" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-4 pr-1">
                        <div class="form-group">
                            <form:label path="teamId">Team:</form:label>
                            <form:select path="teamId" class="form-control">
                                <c:forEach items="${teams}" var="team">
                                    <form:option value="${team.key}">${team.value}</form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="update ml-auto mr-auto">
                        <button type="submit" class="btn btn-primary btn-round">Register</button>
                    </div>
                </div>
                <form:hidden path="id" />
                <form:hidden path="managerId" />
            </form:form>
        </div>
    </div>
</div>

<%@include file="../template/footer.jsp" %>