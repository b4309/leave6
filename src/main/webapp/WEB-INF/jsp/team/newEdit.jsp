<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../template/header.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
    <div class="col-md-12">
        <div class="card-header">
            <h5 class="card-title">
                <c:choose>
                    <c:when test="${team.id eq null}">
                        ثبت اطلاعات تیم جدید
                    </c:when>
                    <c:otherwise>
                        ویرایش اطلاعات ${team.name}
                    </c:otherwise>
                </c:choose>
            </h5>
        </div>
        <div class="card-body">
            <form:form action="/team/add" method="post" modelAttribute="team" >
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <form:label path="name">Name:</form:label>
                            <form:input path="name" class="form-control" placeholder="Name"/>
                        </div>
                    </div>
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <form:label path="code">Code:</form:label>
                            <form:input path="code" class="form-control" placeholder="Code"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="update ml-auto mr-auto">
                        <button type="submit" class="btn btn-primary btn-round">Submit</button>
                    </div>
                </div>
                <form:hidden path="id" />
            </form:form>
        </div>
    </div>
</div>

<%@include file="../template/footer.jsp" %>