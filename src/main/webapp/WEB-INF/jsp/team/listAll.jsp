<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../template/header.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
    <div class="col-md-12">
        <div class="card-header">
            <h5 class="card-title">List All Teams</h5>
            <a href="/team/add" class="btn btn-info">New Team</a>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class=" text-primary">
                <tr>
                    <th>
                        Name
                    </th>
                    <th>
                        Code
                    </th>
                    <th>
                        Tools
                    </th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${teamList}" var="team">
                    <tr>
                        <td>
                            ${team.name}
                        </td>
                        <td>
                                ${team.code}
                        </td>
                        <td>
                            <a href="/team/edit/${team.id}" class="btn btn-warning">Edit</a>
                            <a href="/team/delete/${team.id}" class="btn btn-warning">Delete</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<%@include file="../template/footer.jsp" %>