<%@ page contentType="text/html;charset=UTF-8" %>
<%@include file="../template/header.jsp" %>

<div class="row">
    <h2>متاسفانه شما با خطا مواجه شدید</h2>
    <h5>کد خطای شما: ${code}</h5>
    <h4>متن خطای شما: ${message}</h4>
</div>

<%@include file="../template/footer.jsp" %>