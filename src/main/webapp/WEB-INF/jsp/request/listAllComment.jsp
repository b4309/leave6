<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../template/header.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
    <div class="col-md-12">
        <div class="card-header">
            <h5 class="card-title">List All Comments</h5>
            <a href="/request/add/${requestId}" class="btn btn-info">New Comment</a>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class=" text-primary">
                <tr>
                    <th>
                        Sender
                    </th>
                    <th>
                        Date
                    </th>
                    <th>
                        Subject
                    </th>
                    <th>
                        Text
                    </th>
                    <th>
                        Tools
                    </th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${commentList}" var="comment">
                    <tr>
                        <td>
                            ${comment.sender}
                        </td>
                        <td>
                            ${comment.dateTime}
                        </td>
                        <td>
                            ${comment.subject}
                        </td>
                        <td>
                            ${comment.text}
                        </td>
                        <td>

                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<%@include file="../template/footer.jsp" %>