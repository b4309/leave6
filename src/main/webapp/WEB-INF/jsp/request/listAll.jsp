<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../template/header.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
    <div class="col-md-12">
        <div class="card-header">
            <h5 class="card-title">List All Request</h5>
            <a href="/request/add" class="btn btn-info">New Request</a>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class=" text-primary">
                <tr>
                    <th>
                        Start
                    </th>
                    <th>
                        End
                    </th>
                    <th>
                        Status
                    </th>
                    <th>
                        Employee
                    </th>
                    <th>
                        Tools
                    </th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${requestList}" var="request">
                    <tr>
                        <td>
                                ${request.startDate} ${request.startTime}
                        </td>
                        <td>
                                ${request.endDate} ${request.endTime}
                        </td>
                        <td>
                                ${request.status}
                        </td>
                        <td>
                            <c:choose>
                                <c:when test="${request.employee.gender eq 'm'}">
                                    آقای
                                </c:when>
                                <c:otherwise>
                                    خانم
                                </c:otherwise>
                            </c:choose> ${request.employee.name} ${request.employee.family}
                        </td>
                        <td>
                            <c:if test="${employee.id ne request.employee.id}">
                                <a href="/request/accept/${request.id}" class="btn btn-primary">Accept</a>
                                <a href="/request/reject/${request.id}" class="btn btn-danger">Reject</a>
                            </c:if>
                            <a href="/request/list/${request.id}" class="btn btn-warning">Comments</a>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<%@include file="../template/footer.jsp" %>