<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../template/header.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
    <div class="col-md-12">
        <div class="card-header">
            <h5 class="card-title">
                <c:choose>
                    <c:when test="${comment.id eq null}">
                        ثبت اطلاعات کامنت جدید
                    </c:when>
                    <c:otherwise>
                        ویرایش اطلاعات ${comment.subject}
                    </c:otherwise>
                </c:choose>
            </h5>
        </div>
        <div class="card-body">
            <form:form action="/request/comment/add" method="post" modelAttribute="comment" >
                <div class="row">
                    <div class="col-md-12 pr-1">
                        <div class="form-group">
                            <form:label path="subject">Subject:</form:label>
                            <form:input path="subject" class="form-control" placeholder="Subject"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 pr-1">
                        <div class="form-group">
                            <form:label path="text">Text:</form:label>
                            <form:textarea path="text" class="form-control" placeholder="Text"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="update ml-auto mr-auto">
                        <button type="submit" class="btn btn-primary btn-round">Submit</button>
                    </div>
                </div>
                <form:hidden path="id" />
                <form:hidden path="requestId" />
            </form:form>
        </div>
    </div>
</div>

<%@include file="../template/footer.jsp" %>