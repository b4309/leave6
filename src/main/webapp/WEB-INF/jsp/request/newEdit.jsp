<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@include file="../template/header.jsp" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="row">
    <div class="col-md-12">
        <div class="card-header">
            <h5 class="card-title">
                <c:choose>
                    <c:when test="${request.id eq null}">
                        ثبت اطلاعات درخواست جدید
                    </c:when>
                    <c:otherwise>
                        ویرایش اطلاعات ${request.id}
                    </c:otherwise>
                </c:choose>
            </h5>
        </div>
        <div class="card-body">
            <form:form action="/request/add" method="post" modelAttribute="request" >
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <form:label path="startDate">Start Date:</form:label>
                            <form:input path="startDate" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <form:label path="startTime">Start Time:</form:label>
                            <form:input path="startTime" class="form-control"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <form:label path="endDate">End Date:</form:label>
                            <form:input path="endDate" class="form-control"/>
                        </div>
                    </div>
                    <div class="col-md-6 pr-1">
                        <div class="form-group">
                            <form:label path="endTime">End Time:</form:label>
                            <form:input path="endTime" class="form-control"/>
                        </div>
                    </div>
                </div>
                    <div class="col-md-12 pr-1">
                        <div class="form-group">
                            <form:label path="description">Description:</form:label>
                            <form:textarea path="description" class="form-control" placeholder="Description"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="update ml-auto mr-auto">
                        <button type="submit" class="btn btn-primary btn-round">Submit</button>
                    </div>
                </div>
                <form:hidden path="id" />
                <form:hidden path="employeeId" />
            </form:form>
        </div>
    </div>
</div>

<%@include file="../template/footer.jsp" %>