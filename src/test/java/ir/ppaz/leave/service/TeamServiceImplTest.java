package ir.ppaz.leave.service;

import ir.ppaz.leave.core.LeaveException;
import ir.ppaz.leave.dto.TeamNewEditDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Assert;


import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@AutoConfigureMockMvc
class TeamServiceImplTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    TeamService teamService;

//    @Test
//    void getTeamByIdIgnoreFound() {
//
////        assertNull(teamService.getTeamByIdIgnoreFound(6));
////
////        assertNotNull(teamService.getTeamByIdIgnoreFound(3));
//
//    }

//    @Test
//    void loadTeam() {
//        try {
//            mockMvc.perform(get("/team/list1")).andDo(print()).andExpect(status().isOk());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

//    @Test
//    void getSumAge(){
//        assertEquals(teamService.getSumAge(1),300);
//        assertEquals(teamService.getSumAge(3),700);
//    }

//    @Test
//    void importFromFile(){
//        try {
//            Path path = Paths.get("teamList.txt");
//            if (!Files.exists(path)) {
//                Files.createFile(path);
//                return;
//            }
//            List<String> lines = Files.readAllLines(path);
//
//            List<String> success = new ArrayList<>();
//            List<String> failed = new ArrayList<>();
//
//            lines.forEach(line->{
//                String[] split = line.split(",");
//                if(split.length != 2) {
//                    failed.add(line);
//                    return;
//                }
//                try {
//                    teamService.save(TeamNewEditDto.builder().name(split[0]).code(split[1]).build());
//                    success.add(split[0]);
//                } catch (LeaveException ignored) {
//                    failed.add(split[0]);
//                }
//            });
//        } catch (Exception ignored) {
//
//        }
//    }


}